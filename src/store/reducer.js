import { 
    CHANGE_LOAD,
    SET_ALL_PRODUCTS_TO_STATE, 
    SET_TO_STATE_CURRENT_ELEMENT, 
    SET_TO_BASKET_THIS_PRODUCT, 
    INCREMENT_COUNT, 
    DECREMENT_COUNT,
    REMOVE_ITEM_IN_BASKET
} from "./action"

const initialState = {
    products: [],
    itemsIsOrdered: [],
    currentElement: null,
    load: false,
}


export const rootReducer = (state = initialState, action) => {
    switch (action.type) {
        case SET_ALL_PRODUCTS_TO_STATE:
            return {
                ...state,
                products: action.payload
            }
            break;
        case CHANGE_LOAD:
            return {
                ...state,
                load: action.payload
            }
            break;
        case SET_TO_STATE_CURRENT_ELEMENT:
            return {
                ...state,
                currentElement: action.payload
            }
            break;
        case SET_TO_BASKET_THIS_PRODUCT:
            let index = state.itemsIsOrdered.findIndex(el => el.id === action.payload.id)
            if (index === -1) {
                return {
                    ...state,
                    itemsIsOrdered: [...state.itemsIsOrdered, {...action.payload, count: 1}],
                    products: state.products.map(el => el.id === action.payload.id
                        ? { ...el, rating: { ...el.rating, count: el.rating.count - 1 } }
                        : el
                    )
                }
            } else {
                return {
                    ...state,
                    itemsIsOrdered: state.itemsIsOrdered.map(el => el.id === action.payload.id ?
                        { ...el, count: el.count + 1 }
                        : el
                    ),
                    products: state.products.map(el => el.id === action.payload.id
                        ? { ...el, rating: { ...el.rating, count: el.rating.count - 1 } }
                        : el
                    )
                }
            }

            break;
        case INCREMENT_COUNT:
                return {
                    ...state,
                    itemsIsOrdered: state.itemsIsOrdered.map(el => {
                    return  el.id === action.payload
                        ?
                        { ...el, count: el.count + 1 }
                        : 
                        el
                }),
                    products: state.products.map(el => {
                    return el.id === action.payload
                        ? 
                        { ...el, rating: { ...el.rating, count: el.rating.count - 1 } }
                        :
                        el
                })
                }
            break;
        case DECREMENT_COUNT:
                return {
                    ...state,
                    itemsIsOrdered: state.itemsIsOrdered.map(el => {
                    return  el.id === action.payload
                        ?
                        { ...el, count: el.count - 1 }
                        : 
                        el
                }),
                    products: state.products.map(el => {
                    return el.id === action.payload
                        ? 
                        { ...el, rating: { ...el.rating, count: el.rating.count + 1 } }
                        :
                        el
                })
                }
            break;
            case REMOVE_ITEM_IN_BASKET:
                return {
                    ...state,
                    itemsIsOrdered: state.itemsIsOrdered.filter(el => el.id !== action.payload.id),
                    products: state.products.map(el => {
                        return el.id === action.payload.id
                        ? 
                        {...el, rating: {...el.rating, count: el.rating.count + action.payload.count}}
                        : 
                        el
                    })
                }
                break;
        default: return state
    }
}