export const SET_ALL_PRODUCTS_TO_STATE = 'SET_ALL_PRODUCTS_TO_STATE'
export const SET_TO_BASKET_THIS_PRODUCT = 'SET_TO_BASKET_THIS_PRODUCT'
export const SET_TO_STATE_CURRENT_ELEMENT = 'SET_TO_STATE_CURRENT_ELEMENT'
export const REMOVE_ITEM_IN_BASKET = 'REMOVE_ITEM_IN_BASKET'
export const INCREMENT_COUNT = 'INCREMENT_COUNT'
export const DECREMENT_COUNT = 'DECREMENT_COUNT'
export const CHANGE_LOAD = 'CHANGE_LOAD'


export const get_all_producst = () => (dispatch) => {
    dispatch(change_load(true))
    fetch('https://fakestoreapi.com/products')
        .then(response => response.json())
        .then(products => {
                dispatch(change_load(false))
                dispatch(set_all_products_to_state(products))
        })

} 
export const set_all_products_to_state = (items) => ({
    type: SET_ALL_PRODUCTS_TO_STATE,
    payload: items;
})
export const change_load = (loadBool) => ({
    type: CHANGE_LOAD,
    payload: loadBool
})
export const set_current_element = (element) => ({
    type: SET_TO_STATE_CURRENT_ELEMENT,
    payload: element
})
export const set_to_ordered_product = (element) => ({
    type: SET_TO_BASKET_THIS_PRODUCT,
    payload: element
})
export const increment_count = (id) => ({
    type: INCREMENT_COUNT,
    payload: id
})
export const decrement_count = (id) => ({
    type: DECREMENT_COUNT,
    payload: id
})
export const remove_item_in_basket = (element) => ({
    type: REMOVE_ITEM_IN_BASKET,
    payload: element
})