import React from "react";
import { Route, Switch } from "react-router";
import Home from '../pages/Home'
import {NotFound} from '../pages/404'
import Basket from '../pages/Basket'

export const Routes = () => {
    return <>
    <Switch>
        <Route path='/basket' component={Basket}/>
        <Route path='/' component={Home} exact/>
        <Route path='*' component={NotFound}/>
    </Switch>
    </>
}