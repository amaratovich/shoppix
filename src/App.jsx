 import "materialize-css/dist/css/materialize.min.css";
import { useEffect } from 'react'
import { useDispatch } from 'react-redux'
import { useSelector } from 'react-redux'
import Footer from './components/Footer'
import Header from './components/Header'
import { Routes } from './Routes'
import { get_all_producst } from './store/action'

function App() {

	const state = useSelector(state => state)
	// console.log(state);
	const dispatch = useDispatch()
	
	useEffect(() => {
		dispatch(get_all_producst())
	},[]) 
	
	return <>
	<div className='main'>
		<Header/>
			<Routes/>
		<Footer/>
	</div>
	</>
}

export default App
