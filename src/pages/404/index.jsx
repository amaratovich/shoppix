import React from "react";
import { Link } from "react-router-dom";

export const NotFound = () => {
    return <h1>Not Found... <Link to="/">back to home</Link></h1>
}