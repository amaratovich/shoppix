import React, { useState } from "react";
import { useDispatch } from "react-redux";
import { useSelector } from "react-redux";
import { Modal } from "../../components/Modal";
import { set_current_element, set_to_ordered_product } from "../../store/action";
import './Home.css'

const Home = () => {

    const { products, load } = useSelector(state => state)
    const [modalVisible, setModalVisible] = useState(false)

    const dispatch = useDispatch()

    const setCurrentElementHandler = (element) => {
        dispatch(set_current_element(element))
        setModalVisible(!modalVisible)
    }
    const setElementToBasketHandler = (element) => {
        dispatch(set_to_ordered_product(element))
    }
    const modalVisibleHandler = (event) => {
        setModalVisible(!modalVisible)
    }
    const disabledHandler = (count) => count <= 0 
    return <div className="container">
        {modalVisible && <Modal visibleHandler={modalVisibleHandler} />}
        {
            load

                ?
                <div className="preloader-wrapper big active center">
                    <div className="spinner-layer spinner-blue-only">
                        <div className="circle-clipper left">
                            <div className="circle"></div>
                        </div><div className="gap-patch">
                            <div className="circle"></div>
                        </div><div className="circle-clipper right">
                            <div className="circle"></div>
                        </div>
                    </div>
                </div>

                :
                <div className="row pb">
                    {products.map(el => {
                        return <div className="custom-card" key={el.id}>
                            <div className="card">
                                <div className="card-image">
                                    <img className='img' src={el.image} onClick={() => setCurrentElementHandler(el)} />
                                </div>
                                <div className="card-content">
                                    <p>{el.title}</p>
                                </div>
                                <div className="card-action card-price">
                                    <i className="material-icons">{el.price} $</i>
                                    <b className="material-icons">{el.rating.count} шт.</b>
                                    <a 
                                        className="btn-floating red"
                                        onClick={() => setElementToBasketHandler(el)}
                                        disabled={disabledHandler(el.rating.count)}
                                        >
                                        <i className="material-icons">+</i>
                                    </a>
                                </div>
                            </div>
                        </div>
                    })}
                </div>
        }
    </div>
}
export default Home