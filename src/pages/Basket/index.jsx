import React, { useState } from "react";
import './Basket.css'
import { useSelector } from "react-redux";
import { useDispatch } from "react-redux";
import { decrement_count, increment_count, remove_item_in_basket, set_current_element } from "../../store/action";
import { Alert } from "../../components/Alert";

const Basket = () => {

    const [visibleAlert, setVisibleAlert] = useState(false)

    const { itemsIsOrdered,currentElement } = useSelector(state => state)

    const dispatch = useDispatch()

    const incrementCountHandler = (id) => {
        dispatch(increment_count(id))
    }
    const decrementCountHandler = (id) => {
        dispatch(decrement_count(id))
    }
    const disabledHandler = (totalCount, count) => ({
        incrementButton: Number(totalCount) === count,
        decrementCount: 1 === count
    })
    const alertVisibleHandler = () => {
        setVisibleAlert(!visibleAlert)
    }
    const setCurrentElementHandler = (element) => {
        alertVisibleHandler()
        dispatch(set_current_element(element))
    }
    const removeItemInOrderedItems = (id) => {
        dispatch(remove_item_in_basket(id))
        alertVisibleHandler()
    }

    return <div className='container'>

       {visibleAlert && <Alert 
                            title='are you sure you want to delete ?' 
                            cencelButton 
                            textButton='delete'
                            visible={alertVisibleHandler}
                            click={() => removeItemInOrderedItems(currentElement)}
                            />
       }

        {
            itemsIsOrdered.length
                ? <div className="row pb2">
                    {itemsIsOrdered.map(el => {
                        return <div className="custom-card2" key={el.id}>
                            <div className="card">
                                <div className="card-image">
                                    <img width='400' height='340' src={el.image} />
                                </div>
                                <div className="card-content2">
                                    <p>{el.title}</p>
                                </div>
                                <div className="card-action card-price2">
                                    <i className="material-icons">{el.price} $</i>
                                    <div>
                                        <a 
                                        className="btn-floating red"
                                        onClick={() => decrementCountHandler(el.id)}
                                        disabled={disabledHandler(el.rating.count, el.count).decrementCount}
                                        >
                                            <i className="material-icons">-</i>
                                        </a>
                                            <b 
                                            style={{margin: '0 5px'}} 
                                            className="material-icons"
                                             >{el.count} шт.</b>
                                        <a 
                                        className="btn-floating green"
                                        onClick={() => incrementCountHandler(el.id)}
                                        disabled={disabledHandler(el.rating.count, el.count).incrementButton}
                                        >
                                            <i className="material-icons">+</i>
                                        </a>
                                    </div>
                                    <a className="waves-effect red btn" onClick={() => setCurrentElementHandler(el)}>del</a>
                                    <a className="waves-effect waves-light btn">buy</a>
                                </div>
                            </div>
                        </div>
                    })}
                </div>
                : <h1>Пусто...</h1>
        }
    </div>
}
export default Basket