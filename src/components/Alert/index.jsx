import React from "react";
import './Alert.css'

export const Alert = ({ title, textButton, cencelButton, visible, click }) => {
    return <>
        <div className='alert-opacity-block'></div>
        <div className="alert-body">
            <h5 className='alert-title'>{title}</h5>
            <div className='alert-btns'>
                <div>
                    {
                        cencelButton &&
                        <a
                            className="waves-effect waves-light blue btn"
                            onClick={visible}

                        >cencel</a>
                    }
                </div>
                <a
                    className="waves-effect waves-light red btn"
                    onClick={click}
                >{textButton}</a>
            </div>
        </div>
    </>
}