import React from "react";
import { useSelector } from "react-redux";
import './Modal.css'


export const Modal = ({ visibleHandler }) => {

    const { currentElement } = useSelector(state => state)

    return <div className='main-modal'>
        <div className='modal-opacity' onClick={visibleHandler}></div>
        <div className="modal-body">
            <div className='modal-img'>
                <div>
                    <img width='300' height='350' src={currentElement.image} alt="" />
                </div>
                <div className='modal-price'>
                    <table>
                        <tbody>
                            <tr>
                                <th>{currentElement.category}</th>
                            </tr>
                            <tr>
                                <th>Price: {currentElement.price} $</th>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <div className='modal-description'>
                <p>{currentElement.description}</p>
                <a className="waves-effect waves-light btn custom-btn" onClick={visibleHandler}>close</a>
            </div>
        </div>
    </div>
}