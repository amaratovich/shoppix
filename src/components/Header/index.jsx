import React from 'react'
import { Link } from 'react-router-dom'
import './Header.css'

const Header = () => {
    return <div className='nav'>
        <Link to='/'> <div className='logo'>Shoppix</div></Link>
        <ul className='nav-list'>
            <Link to='/'><li>Home</li></Link>
            <Link to='/basket'><li>Basket</li></Link>
            <Link to='/contacts'><li>Contacts</li></Link>
        </ul>
    </div>
}
export default Header