import React from 'react'
import './Footer.css'

const Footer = () => {
    return <div className='footer'>
            <div className='wegsite'>www.shoppix.kg</div>
            <div className="year">2021.</div>
        </div>
}
export default Footer